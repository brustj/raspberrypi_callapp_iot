const awsIot = require('aws-iot-device-sdk');
const onoff = require('onoff');

const device = awsIot.device({
	keyPath: './certs/66b66f042d-private.pem.key',
	certPath: './certs/66b66f042d-certificate.pem.crt',
	caPath: './certs/AmazonRootCA1.pem',
	clientId: 'RaspberryPi_CallApp_IoT',
	host: 'ahaxehmxwn95b-ats.iot.us-east-1.amazonaws.com'
});
const Gpio = onoff.Gpio;
const led = new Gpio(4, 'out');

device.on( 'connect', () => {
	console.log( 'connected to iot button' );
	
	device.subscribe('iotbutton/G030MD0432227F80');
	
	// device.publish('iotbutton/G030MD0432227F80', JSON.stringify({ test_data: 'NodeJS server connected...'}));
});

device.on( 'message', (topic, payload) => {
	console.log( 'message', topic, payload.toString() );

	const value = (led.readSync() + 1) % 2;
	
  led.write( value, () => {
    console.log( `changed led state to ${value}` );
	});
	
	setTimeout(() => {
		led.write( 0, () => {
			console.log( `auto turning off led` );
		});
	}, 5000);
});

process.on( 'SIGINT', () => {
  led.writeSync(0);
	led.unexport();
	
	console.log( 'iot service disconnected' );
	
  process.exit();
});